#Notas sobre o Git (incluindo servidores remotos)


Criação do projeto:
	
	$git init

Adição de arquivo para ser monitorado:

       $git add arquivo
       
Obter informações sobre o projeto:

      $git status

Criação de uma nova versão:

	$git commit -m "Descrição da nova versão"

Para listar as versões:

     $git log

Para alterar a versão atual:

     $git checkout versão

Para enviar alterações para um servidor remoto:

    $git push -u origin master
